# Persian translation of atk.
# Copyright (C) 2011 Iranian Free Software Users Group (IFSUG.org)translation team.
# Copyright (C) Iranian Free Software Users Group (IFSUG.org)translation team, 2010.
# This file is distributed under the same license as the atk package.
# Roozbeh Pournader <roozbeh@farsiweb.info>, 2003.
# Elnaz Sarbar <elnaz@farsiweb.info>, 2005.
# Meelad Zakaria <meelad@farsiweb.info>, 2005.
# Mahyar Moghimi <mahyar.moqimi@gmail.com>, 2010.
# Arash Mousavi <mousavi.arash@gmail.com>, 2011.
# Danial Behzadi <dani.behzi@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: atk\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/atk/issues\n"
"POT-Creation-Date: 2020-06-06 13:32+0000\n"
"PO-Revision-Date: 2021-09-01 16:30+0430\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Persian <translate@ifsug.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: Poedit 2.4.2\n"

#: atk/atkhyperlink.c:126
msgid "Selected Link"
msgstr "پیوند گزیده"

#: atk/atkhyperlink.c:127
msgid "Specifies whether the AtkHyperlink object is selected"
msgstr "مشخص می‌کند شی AtkHyperlink گزیده شده یا نه"

#: atk/atkhyperlink.c:133
msgid "Number of Anchors"
msgstr "تعداد لنگرها"

#: atk/atkhyperlink.c:134
msgid "The number of anchors associated with the AtkHyperlink object"
msgstr "تعداد لنگرهای وابسته به شیء AtkHyperlink"

#: atk/atkhyperlink.c:142
msgid "End index"
msgstr "نمایهٔ پایان"

#: atk/atkhyperlink.c:143
msgid "The end index of the AtkHyperlink object"
msgstr "نمایهٔ پایان شیء AtkHyperlink"

#: atk/atkhyperlink.c:151
msgid "Start index"
msgstr "نمایهٔ آغاز"

#: atk/atkhyperlink.c:152
msgid "The start index of the AtkHyperlink object"
msgstr "نمایهٔ آغاز شیء AtkHyperlink"

#: atk/atkobject.c:98
msgid "invalid"
msgstr "نامعتبر"

#: atk/atkobject.c:99
msgid "accelerator label"
msgstr "برچسب شتاب‌ده"

#: atk/atkobject.c:100
msgid "alert"
msgstr "آژیر"

#: atk/atkobject.c:101
msgid "animation"
msgstr "پویانمایی"

#: atk/atkobject.c:102
msgid "arrow"
msgstr "پیکان"

#: atk/atkobject.c:103
msgid "calendar"
msgstr "تقویم"

#: atk/atkobject.c:104
msgid "canvas"
msgstr "بوم"

#: atk/atkobject.c:105
msgid "check box"
msgstr "جعبهٔ نشان‌زنی"

#: atk/atkobject.c:106
msgid "check menu item"
msgstr "مورد فهرست نشان‌زنی"

#: atk/atkobject.c:107
msgid "color chooser"
msgstr "گزینشگر رنگ"

#: atk/atkobject.c:108
msgid "column header"
msgstr "سرستون"

#: atk/atkobject.c:109
msgid "combo box"
msgstr "جعبه ترکیب"

#: atk/atkobject.c:110
msgid "dateeditor"
msgstr "ویرایشگر تاریخ"

#: atk/atkobject.c:111
msgid "desktop icon"
msgstr "شمایل رومیزی"

#: atk/atkobject.c:112
msgid "desktop frame"
msgstr "قاب رومیزی"

#: atk/atkobject.c:113
msgid "dial"
msgstr "شماره‌گیری"

#: atk/atkobject.c:114
msgid "dialog"
msgstr "گفت‌وگو"

#: atk/atkobject.c:115
msgid "directory pane"
msgstr "قاب شاخه"

#: atk/atkobject.c:116
msgid "drawing area"
msgstr "ناحیهٔ رسم"

#: atk/atkobject.c:117
msgid "file chooser"
msgstr "گزینشگر پرونده"

#: atk/atkobject.c:118
msgid "filler"
msgstr "پرکننده"

#. I know it looks wrong but that is what Java returns
#: atk/atkobject.c:120
msgid "fontchooser"
msgstr "گزینشگر قلم"

#: atk/atkobject.c:121
msgid "frame"
msgstr "قاب"

#: atk/atkobject.c:122
msgid "glass pane"
msgstr "قاب شیشه‌ای"

#: atk/atkobject.c:123
msgid "html container"
msgstr "بارگنج html"

#: atk/atkobject.c:124
msgid "icon"
msgstr "نقشک"

#: atk/atkobject.c:125
msgid "image"
msgstr "تصویر"

#: atk/atkobject.c:126
msgid "internal frame"
msgstr "قاب داخلی"

#: atk/atkobject.c:127
msgid "label"
msgstr "برچسب"

#: atk/atkobject.c:128
msgid "layered pane"
msgstr "قاب لایه‌دار"

#: atk/atkobject.c:129
msgid "list"
msgstr "سیاهه"

#: atk/atkobject.c:130
msgid "list item"
msgstr "مورد سیاهه"

#: atk/atkobject.c:131
msgid "menu"
msgstr "فهرست"

#: atk/atkobject.c:132
msgid "menu bar"
msgstr "نوار فهرست"

#: atk/atkobject.c:133
msgid "menu item"
msgstr "مورد فهرست"

#: atk/atkobject.c:134
msgid "option pane"
msgstr "قاب گزینه"

#: atk/atkobject.c:135
msgid "page tab"
msgstr "زبانهٔ صفحه"

#: atk/atkobject.c:136
msgid "page tab list"
msgstr "سیاههٔ زبانه‌های صفحه"

#: atk/atkobject.c:137
msgid "panel"
msgstr "تابلو"

#: atk/atkobject.c:138
msgid "password text"
msgstr "متن گذرواژه"

#: atk/atkobject.c:139
msgid "popup menu"
msgstr "فهرست واشو"

#: atk/atkobject.c:140
msgid "progress bar"
msgstr "نوار پیشرفت"

#: atk/atkobject.c:141
msgid "push button"
msgstr "دکمهٔ فشاری"

#: atk/atkobject.c:142
msgid "radio button"
msgstr "دکمهٔ رادیویی"

#: atk/atkobject.c:143
msgid "radio menu item"
msgstr "مورد فهرست رادیویی"

#: atk/atkobject.c:144
msgid "root pane"
msgstr "قاب ریشه"

#: atk/atkobject.c:145
msgid "row header"
msgstr "سرسطر"

#: atk/atkobject.c:146
msgid "scroll bar"
msgstr "نوار لغزش"

#: atk/atkobject.c:147
msgid "scroll pane"
msgstr "قاب لغزش"

#: atk/atkobject.c:148
msgid "separator"
msgstr "جداساز"

#: atk/atkobject.c:149
msgid "slider"
msgstr "لغزنده"

#: atk/atkobject.c:150
msgid "split pane"
msgstr "قاب تقسیم کننده"

#: atk/atkobject.c:151
msgid "spin button"
msgstr "دگمه دوار"

#: atk/atkobject.c:152
msgid "statusbar"
msgstr "نوار وضعیت"

#: atk/atkobject.c:153
msgid "table"
msgstr "جدول"

#: atk/atkobject.c:154
msgid "table cell"
msgstr "خانهٔ جدول"

#: atk/atkobject.c:155
msgid "table column header"
msgstr "سرستون جدول"

#: atk/atkobject.c:156
msgid "table row header"
msgstr "سرسطر جدول"

#: atk/atkobject.c:157
msgid "tear off menu item"
msgstr "مورد فهرست جداشدنی"

#: atk/atkobject.c:158
msgid "terminal"
msgstr "پایانه"

#: atk/atkobject.c:159
msgid "text"
msgstr "متن"

#: atk/atkobject.c:160
msgid "toggle button"
msgstr "دکمهٔ ضامن"

#: atk/atkobject.c:161
msgid "tool bar"
msgstr "نوار ابزار"

#: atk/atkobject.c:162
msgid "tool tip"
msgstr "راهنمای آنی"

#: atk/atkobject.c:163
msgid "tree"
msgstr "درخت"

#: atk/atkobject.c:164
msgid "tree table"
msgstr "جدول درخت"

#: atk/atkobject.c:165
msgid "unknown"
msgstr "نامعلوم"

#: atk/atkobject.c:166
msgid "viewport"
msgstr "دیدگاه"

#: atk/atkobject.c:167
msgid "window"
msgstr "پنجره"

#: atk/atkobject.c:168
msgid "header"
msgstr "سرصفحه"

#: atk/atkobject.c:169
msgid "footer"
msgstr "پاصفحه"

#: atk/atkobject.c:170
msgid "paragraph"
msgstr "پاراگراف"

#: atk/atkobject.c:171
msgid "ruler"
msgstr "خط‌کش"

#: atk/atkobject.c:172
msgid "application"
msgstr "برنامه"

#: atk/atkobject.c:173
msgid "autocomplete"
msgstr "تکمیل خودکار"

#: atk/atkobject.c:174
msgid "edit bar"
msgstr "نوار ویرایش"

#: atk/atkobject.c:175
msgid "embedded component"
msgstr "مؤلفهٔ تعبیه‌شده"

#: atk/atkobject.c:176
msgid "entry"
msgstr "مدخل"

#: atk/atkobject.c:177
msgid "chart"
msgstr "نمودار"

#: atk/atkobject.c:178
msgid "caption"
msgstr "عنوان"

#: atk/atkobject.c:179
msgid "document frame"
msgstr "قاب سند"

#: atk/atkobject.c:180
msgid "heading"
msgstr "سرفصل"

#: atk/atkobject.c:181
msgid "page"
msgstr "صفحه"

#: atk/atkobject.c:182
msgid "section"
msgstr "بخش"

#: atk/atkobject.c:183
msgid "redundant object"
msgstr "شئ زیادی"

#: atk/atkobject.c:184
msgid "form"
msgstr "فرم"

#: atk/atkobject.c:185
msgid "link"
msgstr "پیوند"

#: atk/atkobject.c:186
msgid "input method window"
msgstr "پنجرهٔ روش ورودی"

#: atk/atkobject.c:187
msgid "table row"
msgstr "سطر جدول"

#: atk/atkobject.c:188
msgid "tree item"
msgstr "مورد درخت"

#: atk/atkobject.c:189
msgid "document spreadsheet"
msgstr "سند صفحه‌گسترده"

#: atk/atkobject.c:190
msgid "document presentation"
msgstr "سند ارئه"

#: atk/atkobject.c:191
msgid "document text"
msgstr "سند متنی"

#: atk/atkobject.c:192
msgid "document web"
msgstr "سند وب"

#: atk/atkobject.c:193
msgid "document email"
msgstr "سند پست‌الکترونیکی"

#: atk/atkobject.c:194
msgid "comment"
msgstr "توضیح"

#: atk/atkobject.c:195
msgid "list box"
msgstr "جعبهٔ سیاهه"

#: atk/atkobject.c:196
msgid "grouping"
msgstr "درحال گروه کردن"

#: atk/atkobject.c:197
msgid "image map"
msgstr "نقشه تصویر"

#: atk/atkobject.c:198
msgid "notification"
msgstr "اعلان"

#: atk/atkobject.c:199
msgid "info bar"
msgstr "نوار اطلاعات"

#: atk/atkobject.c:200
msgid "level bar"
msgstr "نوار سطح"

#: atk/atkobject.c:201
msgid "title bar"
msgstr "نوار عنوان"

#: atk/atkobject.c:202
msgid "block quote"
msgstr "نقل قول بلوکی"

#: atk/atkobject.c:203
msgid "audio"
msgstr "صدا"

#: atk/atkobject.c:204
msgid "video"
msgstr "ویدیو"

#: atk/atkobject.c:205
msgid "definition"
msgstr "تعریف"

#: atk/atkobject.c:206
msgid "article"
msgstr "مقاله"

#: atk/atkobject.c:207
msgid "landmark"
msgstr "نشانه"

#: atk/atkobject.c:208
msgid "log"
msgstr "گزارش"

#: atk/atkobject.c:209
msgid "marquee"
msgstr "چادر"

#: atk/atkobject.c:210
msgid "math"
msgstr "ریاضی"

#: atk/atkobject.c:211
msgid "rating"
msgstr "رتبه‌بندی"

#: atk/atkobject.c:212
msgid "timer"
msgstr "زمان‌سنج"

#: atk/atkobject.c:213
msgid "description list"
msgstr "سیاههٔ شرح"

#: atk/atkobject.c:214
msgid "description term"
msgstr "عبارت شرح"

#: atk/atkobject.c:215
msgid "description value"
msgstr "مقدار شرح"

#: atk/atkobject.c:391
msgid "Accessible Name"
msgstr "نام دسترسی‌پذیری"

#: atk/atkobject.c:392
msgid "Object instance’s name formatted for assistive technology access"
msgstr "نام نمونهٔ شیء که برای دسترسی به فناوری یاری‌دهنده قالب‌بندی شده"

#: atk/atkobject.c:398
msgid "Accessible Description"
msgstr "شرح دسترسی‌پذیری"

#: atk/atkobject.c:399
msgid "Description of an object, formatted for assistive technology access"
msgstr "شرح یک شیء، که برای دسترسی به فناوری یاری‌دهنده قالب‌بندی شده"

#: atk/atkobject.c:405
msgid "Accessible Parent"
msgstr "والد دسترسی‌پذیری"

#: atk/atkobject.c:406
msgid "Parent of the current accessible as returned by atk_object_get_parent()"
msgstr ""
"والد دسترسی‌پذیری کنونی، آن‌گونه که به دست ‪atk_object_get_parent()‬ بازگشته"

#: atk/atkobject.c:422
msgid "Accessible Value"
msgstr "مقدار برای دسترسی‌پذیری"

#: atk/atkobject.c:423
msgid "Is used to notify that the value has changed"
msgstr "برای اطلاع دادن از تغییر مقدار استفاده می‌شود"

#: atk/atkobject.c:431
msgid "Accessible Role"
msgstr "نقش دسترسی‌پذیری"

#: atk/atkobject.c:432
msgid "The accessible role of this object"
msgstr "نقش دسترسی‌پذیری این شیء"

#: atk/atkobject.c:439
msgid "Accessible Layer"
msgstr "لایهٔ دسترسی‌پذیری"

#: atk/atkobject.c:440
msgid "The accessible layer of this object"
msgstr "لایهٔ دسترسی‌پذیری این شیء"

#: atk/atkobject.c:448
msgid "Accessible MDI Value"
msgstr "مقدار MDI دسترسی‌پذیری"

#: atk/atkobject.c:449
msgid "The accessible MDI value of this object"
msgstr "مقدار MDI دسترسی‌پذیری این شیء"

#: atk/atkobject.c:465
msgid "Accessible Table Caption"
msgstr "عنوان جدول دسترسی‌پذیری"

#: atk/atkobject.c:466
msgid ""
"Is used to notify that the table caption has changed; this property should "
"not be used. accessible-table-caption-object should be used instead"
msgstr ""
"برای آگاهی از تغییر عنوان جدول استفاده می‌شود. این ویژگی نباید استفاده شود. به "
"جایش باید از accessible-table-caption-object استفاده شود"

#: atk/atkobject.c:480
msgid "Accessible Table Column Header"
msgstr "سرستون جدول دسترسی‌پذیری"

#: atk/atkobject.c:481
msgid "Is used to notify that the table column header has changed"
msgstr "برای اطلاع دادن از تغیر سرستون جدول استفاده می‌شود"

#: atk/atkobject.c:496
msgid "Accessible Table Column Description"
msgstr "شرح ستون جدول برای دسترسی‌پذیری"

#: atk/atkobject.c:497
msgid "Is used to notify that the table column description has changed"
msgstr "برای اطلاع دادن از تغییر شرح ستون جدول استفاده می‌شود"

#: atk/atkobject.c:512
msgid "Accessible Table Row Header"
msgstr "سرسطر جدول دسترسی‌پذیری"

#: atk/atkobject.c:513
msgid "Is used to notify that the table row header has changed"
msgstr "برای اطلاع دادن از تغییر سرسطر جدول استفاده می‌شود"

#: atk/atkobject.c:527
msgid "Accessible Table Row Description"
msgstr "شرح سطر جدول دسترسی‌پذیری"

#: atk/atkobject.c:528
msgid "Is used to notify that the table row description has changed"
msgstr "برای اطلاع دادن از تغییر شرح سطر جدول استفاده می‌شود"

#: atk/atkobject.c:534
msgid "Accessible Table Summary"
msgstr "خلاصهٔ جدول دسترسی‌پذیری"

#: atk/atkobject.c:535
msgid "Is used to notify that the table summary has changed"
msgstr "برای اطلاع دادن از تغییر خلاصه جدول استفاده می‌شود"

#: atk/atkobject.c:541
msgid "Accessible Table Caption Object"
msgstr "شیء عنوان جدول برای دسترسی‌پذیری"

#: atk/atkobject.c:542
msgid "Is used to notify that the table caption has changed"
msgstr "برای اطلاع دادن از تغییر عنوان جدول استفاده می‌شود"

#: atk/atkobject.c:548
msgid "Number of Accessible Hypertext Links"
msgstr "تعداد پیوند‌های Hypertext دسترسی‌پذیری"

#: atk/atkobject.c:549
msgid "The number of links which the current AtkHypertext has"
msgstr "تعداد پیوندهایی که AtkHypertext فعلی دارد"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:194
msgid "very weak"
msgstr "خیلی ضعیف"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:201
msgid "weak"
msgstr "ضعیف"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:208
msgid "acceptable"
msgstr "پذیرفتنی"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:215
msgid "strong"
msgstr "قوی"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:222
msgid "very strong"
msgstr "خیلی قوی"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:230
msgid "very low"
msgstr "خیلی پایین"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:238
msgid "medium"
msgstr "متوسّط"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:246
msgid "high"
msgstr "بالا"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:254
msgid "very high"
msgstr "خیلی بالا"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:262
msgid "very bad"
msgstr "خیلی بد"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:270
msgid "bad"
msgstr "بد"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:278
msgid "good"
msgstr "خوب"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:286
msgid "very good"
msgstr "خیلی خوب"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:294
msgid "best"
msgstr "بهترین"

#~ msgid "Is used to notify that the parent has changed"
#~ msgstr "برای اطلاع دادن از تغییر والد استفاده می‌شود"
